import { Router } from 'express';
import { SurveysController } from './controller/SurveysController';
import { SendMailController } from './controller/SendMailController';
import { UserController } from './controller/UserController';
import { AnswerController } from './controller/AnswerController';
import { NpsController } from './controller/NpsController';

const router = Router();

const userController = new UserController();
const surveysController = new SurveysController();
const sendMailController = new SendMailController();
const answerController = new AnswerController();
const npsController = new NpsController();

router.post('/users', userController.create);
router.get('/users', userController.show);

router.post('/surveys', surveysController.create);
router.get('/surveys', surveysController.show);

router.post('/sendMail', sendMailController.execute);
router.get('/show', sendMailController.show);

router.get('/answers/:value', answerController.execute);

router.get('/nps/:surveys_id', npsController.execute);

export { router };
