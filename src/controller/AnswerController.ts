import { Request, Response } from 'express';
import { getCustomRepository } from 'typeorm';
import AppError from '../errors/AppError';
import { SurveysUserRepository } from '../repositories/SurveysUserRepository';

class AnswerController {
    
    async execute(request: Request, response: Response) {
        const { value } = request.params;
        const { u } = request.query;

        const surveysUserRepository = getCustomRepository(SurveysUserRepository);

        const surveysUser = await surveysUserRepository.findOne({
            id: String(u)
        });

        if (!surveysUser) {
            throw new AppError('Surveys User does not exists!');
        }

        surveysUser.value = Number(value);

        await surveysUserRepository.save(surveysUser);
        
        return response.status(201).json(surveysUser);
    }
}

export { AnswerController };