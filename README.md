# How to clone de project and start

## Clone the project

```console
git clone git@gitlab.com:buratti-experiments/next-level-week-4/next-level-week-4-api-node.git
```

## Install the packages of dependencies of the project

```console
yarn
```

---

## Run the project

* ### First run the migrations to create the structure of the data base

```console
yarn typeorm migration:run
```
* ### Second run the application

```console
yarn dev
```

* ### Third import the collections that meet in directory **collections** in your tool of API test, the collections are exported of the insomnia version 4 in extensions **.json**, **.yml** (yaml) and **.har** (HTTP archive format)

* ### Fourth test the endpoints

* ### Another alternative is if you vs-code use, you can install the **RestClient** extension, and use the files that are in the **requests** directory

---

## How were created project with structure Node using Yarn

```console
mkdir api-node
cd api-node
yarn init -y
```

---

## How to were install the packages used their project

```console
yarn add express
yarn add @types/express -D
yarn add typescript -D
yarn tsc --init
yarn add ts-node-dev -D
yarn add typeorm reflect-metadata
yarn add sqlite3
yarn add uuid
yarn add @types/uuid -D
yarn add jest @types/jest -D
yarn jest --init
yarn add ts-jest -D
yarn add supertest @types/supertest -D
yarn add nodemailer
yarn add @types/nodemailer -D
yarn add handlebars
yarn add yup
yarn add express-async-errors
```

---

## Commands to create the migrations of the project

```console
yarn typeorm migration:create -n CreateUsers 
yarn typeorm migration:run 
yarn typeorm migration:create -n CreateSurveys 
yarn typeorm migration:run 
yarn typeorm migration:create -n CreateSurveysUsers
yarn typeorm migration:run 
```
